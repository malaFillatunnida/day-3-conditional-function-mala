function minuman(email, money, drinkChoice = "coffee") {

  let harga;

  switch (drinkChoice) {
    case "mineral water":
      harga = money - 5000;
      break;
    case "cola":
      harga = money - 7500;
      break;
    case "coffee":
      harga = money - 12250;
      break;
    default:
      harga = money;
      break;
  }

  if (!email) {
    console.log("Sorry can't process it until your email filled");
  } else if (typeof email != "string") {
    console.log("Invalid input");
  } else if (!money) {
    console.log("Sorry can't process it until you put your money");
  } else if (typeof money != "number") {
    console.log("Invalid input");
  } else if (email != null && money != null && harga > 0) {
    console.log(`Welcome ${email} to May's Vending Machine`);
    console.log(`Your choice is ${drinkChoice}, here's your drink`);
    console.log(`Your changes are ${harga}`);
    console.log("Thank you");
  } else if (email != null && money != null && harga < 0) {
    console.log(`Welcome ${email} to May's Vending Machine`);
    console.log(`Sorry, insufficient balance we can't process your ${drinkChoice}`);
    console.log(`You need ${harga *= -1} more to buy this drink`);
    console.log("Thank you");
  } else {
    console.log("Please check your input");
  }
}

minuman("mala@gmail.com", 4000);
